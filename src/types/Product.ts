export default interface Product {
  id?: number;

  name: string;

  price: number;

  createAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
